from django.db import models
from martor.models import MartorField


class PublishedManager(models.Manager):
    def get_queryset(self):
        return super(PublishedManager, self).get_queryset() \
                                            .filter(status='published')


class Tag(models.Model):
    title = models.CharField(max_length=100, db_index=True)
    slug = models.SlugField(max_length=120, unique=True, db_index=True)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'tags'


class Article(models.Model):
    STATUS_CHOICES = (
        ('draft', 'Draft'),
        ('published', 'Published'),
    )

    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=120, unique=True)
    date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='draft')
    tags = models.ManyToManyField(Tag)
    content = MartorField()

    # Managers
    objects = models.Manager()
    published = PublishedManager()

    def __str__(self): 
        return self.title

    class Meta:
        db_table = 'articles'
        ordering = ['-date']