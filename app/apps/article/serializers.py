from rest_framework import serializers
from apps.article.models import Article, Tag


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'title', 'slug')


class ArticlePreviewSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    content = serializers.SerializerMethodField()
    date = serializers.DateTimeField(format='%d/%m/%Y %H:%M')

    class Meta:
        model = Article
        fields = ('id', 'slug', 'tags', 'content', 'title', 'date')

    def get_content(self, obj):
        p_tag = obj.content.find('\r\n\r\n')
        return obj.content if p_tag == -1 else obj.content[:p_tag]


class ArticleSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    date = serializers.DateTimeField(format='%d/%m/%Y %H:%M')

    class Meta:
        model = Article
        fields = ('id', 'slug', 'tags', 'content', 'title', 'date')
