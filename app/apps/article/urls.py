from django.urls import path
from . import views

app_name = 'articles'

urlpatterns = [
    path('articles/', views.ArticlesAPIView.as_view({'get': 'list'})),
    path('articles/article/<str:slug>/', views.ArticlesAPIView.as_view({'get': 'retrieve'})),
    path('articles/tag/<str:slug>/', views.TagArticlesAPIViews.as_view()),
    path('tags/', views.AllTagsAPIView.as_view())
]
