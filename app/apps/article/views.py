from django.shortcuts import get_object_or_404
from rest_framework import mixins, viewsets, generics
from .models import Article, Tag
from .serializers import ArticlePreviewSerializer, ArticleSerializer, TagSerializer


class ArticlesAPIView(
    viewsets.GenericViewSet,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin
):
    serializer_class = ArticlePreviewSerializer
    queryset = Article.published.all()
    lookup_field = 'slug'

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return ArticlePreviewSerializer
        return ArticleSerializer


class AllTagsAPIView(generics.ListAPIView):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer


class TagArticlesAPIViews(generics.ListAPIView):
    serializer_class = ArticleSerializer

    def get_queryset(self):
        tag = get_object_or_404(Tag, slug=self.kwargs['slug'])
        return Article.published.filter(tags=tag)
