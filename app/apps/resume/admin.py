from django.db import models
from django.contrib import admin
from martor.widgets import AdminMartorWidget
from apps.resume.models import Experience


@admin.register(Experience)
class ArticleAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': AdminMartorWidget},
    }
