# Generated by Django 3.0.2 on 2020-02-04 05:44

from django.db import migrations, models
import martor.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Experience',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('position', models.CharField(max_length=100)),
                ('company', models.CharField(max_length=100)),
                ('from_date', models.CharField(max_length=30)),
                ('to_date', models.CharField(max_length=30)),
                ('tools', models.CharField(max_length=255)),
                ('description', martor.models.MartorField()),
                ('priority', models.PositiveIntegerField(default=1)),
            ],
            options={
                'db_table': 'experience',
                'ordering': ['priority', 'id'],
            },
        ),
    ]
