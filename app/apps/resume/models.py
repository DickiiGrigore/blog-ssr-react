from django.db import models
from martor.models import MartorField


class Experience(models.Model):
    position = models.CharField(max_length=100)
    company = models.CharField(max_length=100)
    from_date = models.CharField(max_length=30)
    to_date = models.CharField(max_length=30)
    tools = models.CharField(max_length=255)
    description = MartorField()
    priority = models.PositiveIntegerField(default=1)

    def __str__(self): 
        return f'{self.position} ({self.company})'

    class Meta:
        db_table = 'experience'
        ordering = ['-priority', 'id']