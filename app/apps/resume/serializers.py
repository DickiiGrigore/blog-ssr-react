from rest_framework import serializers
from apps.resume.models import Experience


class ExperienceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Experience
        exclude = ('priority', )
