from django.shortcuts import get_object_or_404
from rest_framework import generics
from .models import Experience
from .serializers import ExperienceSerializer


class ExpirienceAPIView(generics.ListAPIView):
    queryset = Experience.objects.all()
    serializer_class = ExperienceSerializer 
