"""blog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from .views import ImageUploadView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('martor/', include('martor.urls'))
]

if settings.DEBUG:
    # Serve on dev
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# Api
urlpatterns += [
    path('api/blog/', include('apps.article.urls', namespace='blog-api')),
    path('api/resume/', include('apps.resume.urls', namespace='resume-api')),
    path('api/uploader/', ImageUploadView.as_view() , name='image_uploader'),
]
