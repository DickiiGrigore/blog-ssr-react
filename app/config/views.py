import os
import json
import uuid
from datetime import datetime

from django.conf import settings
from django.http import HttpResponse
from django.views.generic.base import View
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.decorators import login_required
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.utils.decorators import method_decorator
from martor.utils import LazyEncoder


@method_decorator(login_required, name='dispatch')
class ImageUploadView(View):
    image_types = [
        'image/png', 'image/jpg',
        'image/jpeg', 'image/pjpeg', 'image/gif'
    ]

    def post(self, *args, **kwargs):
        """
        Makdown image upload for locale storage
        and represent as json to markdown editor.
        """
        if self.request.is_ajax():
            if 'markdown-image-upload' in self.request.FILES:
                image = self.request.FILES['markdown-image-upload']
                if image.content_type not in self.image_types:
                    data = json.dumps({
                        'status': 405,
                        'error': _('Bad image format.')
                    }, cls=LazyEncoder)
                    return HttpResponse(
                        data, content_type='application/json', status=405)

                if image.size > settings.MAX_IMAGE_UPLOAD_SIZE:
                    to_MB = settings.MAX_IMAGE_UPLOAD_SIZE / (1024 * 1024)
                    data = json.dumps({
                        'status': 405,
                        'error': _('Maximum image file is %(size) MB.') % {'size': to_MB}
                    }, cls=LazyEncoder)
                    return HttpResponse(
                        data, content_type='application/json', status=405)

                now = datetime.now()
                img_uuid = f"{uuid.uuid4().hex[:10]}-{image.name.replace(' ', '-')}"
                tmp_file = os.path.join(settings.MARTOR_UPLOAD_PATH, str(now.year), str(now.month), str(now.day), img_uuid)
                def_path = default_storage.save(tmp_file, ContentFile(image.read()))
                img_url = os.path.join(settings.MEDIA_URL, def_path)

                data = json.dumps({
                    'status': 200,
                    'link': img_url,
                    'name': image.name
                })

                return HttpResponse(data, content_type='application/json')
            return HttpResponse(_('Invalid request!'))
        return HttpResponse(_('Invalid request!'))
