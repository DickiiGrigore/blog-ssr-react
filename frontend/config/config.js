const path = require('path');
const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const { NODE_ENV = 'development'} = process.env;
const IS_DEVELOPMENT = NODE_ENV === 'development';

function createTarger({ target }) {
    const IS_SERVER = target === 'server';
    const IS_CLIENT = target === 'client';
    const root =  path.join(__dirname, '../');
    const dist = path.join(root, 'dist', target);
    const src = path.join(root, 'src');
    const filename = ext => IS_DEVELOPMENT ? `[name].${ext}` : `[name].[hash].${ext}`;

    return {
        root,
        dist,
        src,
        isDevelopment: IS_DEVELOPMENT,
        webpack: {
            mode: NODE_ENV || 'development',
            devtool: IS_DEVELOPMENT ? 'source-map' : false,
            entry: path.join(src, target),
            stats: 'minimal',
            watch: IS_DEVELOPMENT,
            output: {
                path: dist,
                filename: filename('js'),
                chunkFilename: filename('js'),
                publicPath: IS_DEVELOPMENT ? '' : process.env.PUBLIC_PATH
            },
            stats: {
                entrypoints: true
            },
            module: {
                rules: [
                    {
                        test: /\.(js|jsx)$/,
                        exclude: /node_modules/,
                        use: {
                            loader: 'babel-loader',
                            options: {
                                presets: ['@babel/preset-env', '@babel/preset-react'],
                                plugins: ['@babel/plugin-transform-runtime', '@babel/plugin-proposal-class-properties']
                            }
                        }
                    }
                ]
            },
            plugins: [
                new webpack.DefinePlugin({
                    IS_SERVER: JSON.stringify(IS_SERVER),
                    IS_CLIENT: JSON.stringify(IS_CLIENT),
                    DOMAIN: JSON.stringify(process.env.DOMAIN),
                    API_URL: JSON.stringify(IS_CLIENT ? process.env.API_URL_CLIENT : process.env.API_URL_SERVER),
                    ...(IS_SERVER && {
                        window: JSON.stringify('undefined')
                    })
                }),
                new webpack.NoEmitOnErrorsPlugin(),
                ...(!IS_DEVELOPMENT ? [new UglifyJSPlugin()] : [])
            ]
        }
    }
}

module.exports = createTarger;