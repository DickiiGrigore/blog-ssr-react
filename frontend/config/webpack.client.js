const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackAssetsManifest = require('webpack-assets-manifest');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetWebpackPlugin = require('optimize-css-assets-webpack-plugin');
const TerserWebpackPlugin = require('terser-webpack-plugin');
const config = require('./config')({
    target: 'client'
});

const cssLoaders = extra => {
    return [
        {
            loader: MiniCssExtractPlugin.loader,
            options: {
                hmr: config.isDevelopment,
                reloadAll: config.isDevelopment
            }
        },
        'css-loader',
        ...(extra ? [extra] : [])
    ];;
};

module.exports = {
    ...config.webpack,
    module: {
        ...config.webpack.module,
        rules: [
            ...config.webpack.module.rules,
            {
                test: /\.css$/,
                use: cssLoaders()
            },
            {
                test: /\.s[ac]ss$/,
                use: cssLoaders('sass-loader')
            },
            {
                test: /\.(jpe?g|png|gif|svg|ico)$/i,
                use: ['file-loader']
            }
        ]
    },
    devServer: {
        host: process.env.HOST,
        port: process.env.PORT,
        hot: config.isDevelopment,
        stats: 'errors-only',
        historyApiFallback: true,
        publicPath: '/'
    },
    optimization: {
        minimizer: !config.isDevelopment ? [
            new OptimizeCssAssetWebpackPlugin(),
            new TerserWebpackPlugin()
        ] : []
    },
    plugins: [
        ...config.webpack.plugins,
        new MiniCssExtractPlugin({
            filename: config.isDevelopment ? '[name].css' : '[name].[hash].css'
        }),
        new CleanWebpackPlugin(),
        new WebpackAssetsManifest({
            publicPath: true,
            entrypoints: true
        }),
        ...(config.isDevelopment ? [
            new HtmlWebpackPlugin({template: './src/index.html'})
        ] : [])
    ]
}