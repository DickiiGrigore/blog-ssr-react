const nodeExternals = require('webpack-node-externals');
const config = require('./config')({
    target: 'server'
});

module.exports = {
    ...config.webpack,
    target: 'node',
    output: {
        ...config.webpack.output,
        filename: 'server.compiled.js',
        libraryTarget: 'commonjs2'
    },
    module: {
        ...config.webpack.module,
        rules: [
            ...config.webpack.module.rules,
            {
                test: /\.css$/,
                use: ['null-loader']
            },
            {
                test: /\.s[ac]ss$/,
                use: ['null-loader']
            },
            {
                test: /\.(jpe?g|png|gif|svg|ico)$/i,
                use: ['file-loader?emitFile=false']
            },
        ]
    },
    externals: [nodeExternals()]
}