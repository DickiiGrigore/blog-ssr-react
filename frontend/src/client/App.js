import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Main from './core/Main';
import routes from './core/routes';
import { configureApp } from './core/config';

configureApp();

const App = () => (
    <Main>
        <Switch>
            {routes.map(route => (
                <Route key={route.path} {...route} />
            ))}
        </Switch>
    </Main>
);

export default App;