import React from 'react';
import { Alert } from 'react-bootstrap';
import ShortArticle from './ShortArticle';

export default function ArticlesList({ articles }) {
    return articles.length ?
                articles.map(article => <ShortArticle key={article.id} article={article} />) :
                <Alert variant="secondary">Статьи не найдены!</Alert>
};