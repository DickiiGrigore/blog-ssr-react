import React from 'react';
import { Link } from 'react-router-dom';
import ReactMarkdown from 'react-markdown';

export default function ShortArticle({ article }) {
    return (
        <div className="article-short article mb-4">
            <Link to={`/blog/article/${article.slug}/`}><h3>{article.title}</h3></Link>
            <div className="subheading mb-3">
                <span className="article-date">{article.date}</span>
                <span className="article-tags">
                    {article.tags.map(tag => (
                        <span key={tag.id} className="tag"><Link to={`/blog/tag/${tag.slug}/`}>#{tag.title}</Link></span>
                    ))}
                </span>
            </div>
            <div className="article-content">
                <ReactMarkdown source={article.content} />
            </div>
        </div>
    );
};