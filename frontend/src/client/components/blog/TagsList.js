import React from 'react';
import { Link } from 'react-router-dom';

export default function TagsList({ tags, activeTag }) {
    return (
        <div className="subheading mb-5">
            <div className="article-tags">
                {tags.map(tag => (
                    <span key={tag.id} className="tag">{
                        activeTag && activeTag.slug == tag.slug ? (
                            <span>#{tag.title}</span>
                        ) : (
                            <Link to={`/blog/tag/${tag.slug}`}>#{tag.title}</Link>
                        )
                    }</span>
                ))}
            </div>
        </div>
    );
};