import React from 'react';
import { Helmet } from  'react-helmet';
import { Navbar, Nav } from 'react-bootstrap';
import { NavLink, Link } from 'react-router-dom';
import profile from './profile.jpg';
import favicon from './favicon.ico';
import './header.scss';

export default function Header() {
    return (
        <>
            <Helmet>
                <title>Dikii Grigore - dikiigr.ru</title>
                <link rel="icon" type="image/png" href={favicon} sizes="16x16" />
            </Helmet>
            <Navbar id="sideNav" collapseOnSelect expand="lg" bg="base" variant="dark">
                <Navbar.Brand>
                    <Link to="/">
                        <span className="d-block d-lg-none blog-title">Dikii Grigore</span>
                        <span className="d-none d-lg-block">
                            <img className="img-fluid img-profile rounded-circle mx-auto mb-2" src={profile} alt=""/>
                        </span>
                    </Link>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <NavLink exact={true} to="/" className="nav-link">Блог</NavLink>
                        <NavLink exact={true} to="/resume" className="nav-link">Skills & Experience</NavLink>
                        <NavLink exact={true} to="/contacts" className="nav-link">Контакты</NavLink>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </>
    );
};