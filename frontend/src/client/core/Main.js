import React from 'react';
import Container from 'react-bootstrap/Container';
import Header from '../components/header/Header';

export default function Main(props) {
    return (
        <>
            <Header />
            <Container fluid className="p-0">
                <div className="page p-3 p-lg-5 d-flex align-items-center">
                    {props.children}
                </div>
            </Container>
        </>
    );
};