import axios from 'axios';

export const configureApp = () => {
    axios.defaults.baseURL = API_URL;
};