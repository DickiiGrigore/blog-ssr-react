import React, { Component } from 'react';
import { Spinner } from 'react-bootstrap';

export default ({
    key,
    getData,
    initState = {}
}) => (WrappedComponent) => {
    class SSRCompatibleComponent extends Component {
        constructor(props) {
            super(props);
            this.state = {
                ...initState,
                ...this.getSSRState(this.props)
            };
        }

        componentDidMount() {
            if (!this.state.loaded) {
                this.loadDataAndSetToStore();
            }
        }

        getSSRState(props) {
            let ssrState = {loaded: false};
            const staticContext = props.staticContext;

            if (staticContext && staticContext.data && Object.keys(staticContext.data).includes(key)) {
                const data = staticContext.data[key];
                ssrState = {...data, loaded: true};
            } else if (window && window.__DATA__ && Object.keys(window.__DATA__).includes(key)) {
                const data = window.__DATA__[key];
                delete window.__DATA__[key];
                ssrState = {...data, loaded: true};
            }

            return ssrState
        }

        async loadDataAndSetToStore() {
            const data = await getData(this.props);
            this.setState({...data, loaded: true});
        }

        render() {
            return !this.state.loaded ? (
                <Spinner animation="border" />
            ) : (
                <WrappedComponent
                    {...this.props}
                    {...this.state}
                />
            );
        }
    }

    SSRCompatibleComponent.getData = getData;
    SSRCompatibleComponent.key = key;

    return SSRCompatibleComponent;
}