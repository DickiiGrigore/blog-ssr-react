import ContactsPage from '../pages/contacts/ContactsPage';
import ResumePage from '../pages/resume/ResumePage';
import HomePage from '../pages/home/HomePage';
import homeRoutes from '../pages/home/routes';

export default [
    {
        path: '/contacts',
        component: ContactsPage,
        exact: true,
    },
    {
        path: '/resume',
        component: ResumePage,
        exact: true,
    },
    {
        path: '/',
        component: HomePage,
        routes: homeRoutes
    }
];