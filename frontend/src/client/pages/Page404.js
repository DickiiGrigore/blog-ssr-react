import React from 'react';
import { Alert } from 'react-bootstrap';

export default function Page404(props) {
    return (
        <div id="page-404">
            <h1 className="mb-5">Page <span className="text-primary">404</span></h1>
            <Alert variant="secondary">Ничего не найдено</Alert>
        </div>
    );
}