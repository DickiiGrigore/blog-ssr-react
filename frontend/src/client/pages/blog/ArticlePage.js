import axios from 'axios';
import ssr from '../../core/hocs/ssr';
import ArticlePageComponent from './ArticlePageComponent';

export default ssr({
    key: 'fullArticle',
    getData: async ({ match: { params } }) => {
        try {
            const { data } = await axios.get(`/blog/articles/article/${params.slug}/`);
            return { article: data };
        } catch (error) {
            console.error(`Failed to load article: ${error}`);
        }
    },
    initState: {
        article: null,
    }
})(ArticlePageComponent);