import React from 'react';
import { Helmet } from  'react-helmet';
import { Link } from 'react-router-dom';
import ReactMarkdown from 'react-markdown';
import Disqus from 'disqus-react';
import CodeBlock from '../../components/blog/CodeBlock';

export default function ArticlePageComponent({ article, match }) {
    return (
        <>
            <Helmet>
                <title>{article.title} | Dikii Grigorii</title>
            </Helmet>
            <div className="article-short article mb-4">
                <h1>{article.title}</h1>
                <div className="subheading mb-3">
                    <span className="article-date">{article.date}</span>
                    <span className="article-tags">
                        {article.tags.map(tag => (
                            <span key={tag.id} className="tag"><Link to={`/blog/tag/${tag.slug}/`}>#{tag.title}</Link></span>
                        ))}
                    </span>
                </div>
                <div className="article-content">
                    <ReactMarkdown source={article.content} renderers={{code: CodeBlock}} />
                </div>
                <Disqus.DiscussionEmbed shortname="dikiigrigorii" config={{
                    url: `${DOMAIN}${match.url}`,
                    identifier: `${article.slug}-${article.id}`,
                    title: article.title
                }}/>
            </div>
        </>
    );
};