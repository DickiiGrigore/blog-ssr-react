import React from 'react';
import { Helmet } from  'react-helmet';
import ArticlesList from '../../components/blog/ArticlesList';
import TagsList from '../../components/blog/TagsList';

export default function ArticlesPageComponent({ title, articles, tags, tag }) {
    return (
        <>
            <Helmet>
                <title>{title} | Dikii Grigorii</title>
            </Helmet>
            <div className="all-articles">
                <h1>{title}</h1>
                <TagsList tags={tags} activeTag={tag}/>
                <ArticlesList articles={articles} />
            </div>
        </>
    );
};