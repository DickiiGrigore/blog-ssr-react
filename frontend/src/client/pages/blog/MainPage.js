import axios from 'axios';
import ssr from '../../core/hocs/ssr';
import ArticlesPageComponent from './ArticlesPageComponent';
import './blog.scss';

const loadArticles = async () => {
    try {
        const response = await axios.get('/blog/articles/');
        return response.data;
    } catch (error) {
        console.error(`Failed to load articles: ${error}`);
    }
}

const loadTags = async () => {
    try {
        const response = await axios.get('/blog/tags/');
        return response.data;
    } catch (error) {
        console.error(`Failed to load tags: ${error}`);
    }
}

export default ssr({
    key: 'allArticles',
    getData: async () => {
        const articles = await loadArticles();
        const tags = await loadTags();
        return { articles, tags };
    },
    initState: {
        articles: [],
        tags: [],
        title: 'Все статьи'
    }
})(ArticlesPageComponent);