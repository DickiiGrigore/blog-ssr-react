import React, { Component } from 'react';
import axios from 'axios';
import { Spinner } from 'react-bootstrap';
import ssr from '../../core/hocs/ssr';
import ArticlesPageComponent from './ArticlesPageComponent';

const loadResources = async ({ match: {params} }) => {
    const articles = await loadArticles(params.slug);
    const tags = await loadTags();
    const tag = tags.find(t => t.slug === params.slug)
    return { articles, tags, tag };
};

const loadArticles = async (tagSlug) => {
    try {
        const response = await axios.get(`/blog/articles/tag/${tagSlug}/`);
        return response.data;
    } catch (error) {
        console.error(`Failed to load article: ${error}`);
    }
};

const loadTags = async () => {
    try {
        const response = await axios.get('/blog/tags/');
        return response.data;
    } catch (error) {
        console.error(`Failed to load articles: ${error}`);
    }
};

class TagArticlesContainer extends Component {
    state = {};

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.slug !== this.props.match.params.slug) {
            this.loadResourcesForTag();
        }
    }

    async loadResourcesForTag() {
        this.setState({loaded: false});
        const data = await loadResources(this.props);
        this.setState({...data, loaded: true});
    }

    render() {
        return !this.state.loaded ? (
            <Spinner animation="border" />
        ) : (
            <ArticlesPageComponent {...this.state} title={`#${this.state.tag.title}`}/>
        );
    }
};

TagArticlesContainer.getDerivedStateFromProps = (props, prevState) => {
    if (!Object.keys(prevState).length) {
        return {
            articles: props.articles,
            tags: props.tags,
            tag: props.tag,
            loaded: true
        }
    }

    return {};
};

export default ssr({
    key: 'tagArticles',
    getData: loadResources,
    initState: {
        tag: null,
        articles: [],
        tags: []
    }
})(TagArticlesContainer);