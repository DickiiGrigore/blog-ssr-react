import React from 'react';
import { Helmet } from  'react-helmet';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faVk, faGithub, faFacebookF, faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
import { Button, ButtonToolbar } from 'react-bootstrap';
import './contacts.scss';

export default function ContactsPage(props) {
    return (
        <>
            <Helmet>
                <title>Контакты | Dikii Grigorii</title>
            </Helmet>
            <div id="about">
                <h1 className="mb-0">Dikii <span className="text-primary">Grigore</span></h1>
                <div className="subheading mb-5">Full Stack Developer · <a href="mailto:dikiigr@gmail.com" target="_blank">dikiigr@gmail.com</a></div>
                <p className="lead mb-3">
                    <b>Full-Stack</b> разработчик со стажем более 5 лет. Свободен для удаленной работы, при необходимости готов к переезду. 
                    Для вопросов и предложений пишите на <a href="mailto:dikiigr@gmail.com" target="_blank">dikiigr@gmail.com</a> или используйте одну из 
                    соц-сетей ниже.
                </p>
                <div className="resume-button mb-5">
                    <ButtonToolbar>
                        <Button variant="primary" className="mr-2" href="https://www.dropbox.com/s/6wvxskb6u0r0wh5/Resume_ENG.docx" target="_blanck">
                            Resume ENG
                        </Button>
                        <Button variant="primary" href="https://www.dropbox.com/s/rcmr91runmhgdya/Resume_RUS.docx" target="_blanck">
                            Resume RUS
                        </Button>
                    </ButtonToolbar>
                </div>
                <div className="social-icons">
                    <a href="https://github.com/dikiigr" target="_blank">
                        <FontAwesomeIcon icon={faGithub} />
                    </a>
                    <a href="https://www.linkedin.com/in/dikiigr/" target="_blank">
                        <FontAwesomeIcon icon={faLinkedinIn} />
                    </a>
                    <a href="https://www.facebook.com/profile.php?id=100005363896313" target="_blank">
                        <FontAwesomeIcon icon={faFacebookF} />
                    </a>
                    <a href="https://vk.com/dikiigrigorii" target="_blank">
                        <FontAwesomeIcon icon={faVk} />
                    </a>
                </div>
            </div>
        </>
    );
};