import React from 'react';
import { Switch, Route } from 'react-router-dom';
import routes from './routes';

export default function HomePage() {
    return (
        <div id='blog'>
            <Switch>
                {routes.map(route => (
                    <Route key={route.path || '404'} {...route} />
                ))}
            </Switch>
        </div>
    );
};