import MainPage from '../blog/MainPage';
import TagPage from '../blog/TagPage';
import ArticlePage from '../blog/ArticlePage';
import Page404 from '../Page404';

export default [
    {
        path: '/blog/article/:slug',
        component: ArticlePage,
        exact: true
    },
    {
        path: '/blog/tag/:slug',
        component: TagPage,
        exact: true
    },
    {
        path: '/',
        component: MainPage,
        exact: true
    },
    {
        component: Page404
    }
];