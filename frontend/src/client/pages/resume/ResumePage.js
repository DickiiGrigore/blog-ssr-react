import axios from 'axios';
import ssr from '../../core/hocs/ssr';
import ResumePageComponent from './ResumePageComponent';

export default ssr({
    key: 'resume',
    getData: async () => {
        try {
            const response = await axios.get('/resume/expirience/');
            return {experience: response.data};
        } catch (error) {
            console.error(`Failed to load experience: ${error}`);
        }
    },
    initState: {
        experience: []
    }
})(ResumePageComponent);
