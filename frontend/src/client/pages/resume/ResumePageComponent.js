import React from 'react';
import { Helmet } from  'react-helmet';
import { Row, Col, Button, ButtonToolbar } from 'react-bootstrap';
import ReactMarkdown from 'react-markdown'
import './resume.scss';

export default function ResumeComponent({ experience }) {
    return (
        <>
            <Helmet>
                <title>Resume | Dikii Grigorii</title>
            </Helmet>
            <div id="skills">
                <section className="skills mb-5">
                    <h1>Skills</h1>
                    <div className="subheading mb-3">Back-end</div>
                    <p className="lead mb-5">В качестве языков программирования использую <b>Python</b> или <b>NodeJs</b>. Из фреймворков использую <b>Django</b>, <b>Flask</b> и <b>Express</b>. Для конкурентного программирования на <b>Python</b> использую <b>aiohttp</b> или <b>sanic</b>.</p>
                    <div className="subheading mb-3">Front-end</div>
                    <p className="lead mb-5">В качестве библиотеки для работы с интерфейсами использую <b>React</b>, в зависимости от проекта поднимаю и настраиваю <b>SSR</b>. Иногда использую <b>JQuery</b>. Собираю все через <b>webpack</b>.</p>
                    <div className="subheading mb-3">Workflow</div>
                    <p className="lead">При сборке проектов использую <b>Docker</b>, для сборки front-end <b>webpack</b>, в качестве системы контроля версий <b>git</b>. Пытаюсь следовать принципам <b>SOLID</b>, <b>YAGNI</b>, <b>KISS</b> и <b>DRY</b>.</p>
                </section>
                <h2>Experience</h2>
                <section className="resume mb-5">
                    {experience.map(e => (
                        <div key={e.id} className="resume-item mb-5">
                            <Row>
                                <Col md={8}>
                                    <div className="resume-header">
                                        <h3>{e.position}</h3>
                                        <div className="subheading mb-3">{e.company}</div>
                                    </div>
                                </Col>
                                <Col md={4}>
                                    <div className="resume-date text-md-right">
                                        <span className="text-primary">{e.from_date} - {e.to_date}</span>
                                    </div>
                                </Col>
                            </Row>
                            <div className="resume-content lead">
                                <ReactMarkdown source={e.description} />
                                <p><b>Использовал:</b> {e.tools}</p>
                            </div>
                        </div>
                    ))}
                </section>
                <h2>Education</h2>
                <section className="education mb-5">
                    <div className="subheading mb-3"> БГТУ "ВОЕНМЕХ" им. Д.Ф. Устинова, Санкт-Петербург</div>
                    <div className="education-content lead">
                        Обучался в Балтийском государственном техническом университете «Военмех» имени Дмитрия Федоровича Устинова на факультете <b>«И» Информационные и Управляющие системы</b> кафедры <b>Программной инженерии</b>. Закончил с <b>отличием</b>.
                    </div>
                </section>
                <section className="resume-button">
                    <ButtonToolbar>
                        <Button variant="primary" className="mr-2" href="https://www.dropbox.com/s/6wvxskb6u0r0wh5/Resume_ENG.docx" target="_blanck">
                            Resume ENG
                        </Button>
                        <Button variant="primary" href="https://www.dropbox.com/s/rcmr91runmhgdya/Resume_RUS.docx" target="_blanck">
                            Resume RUS
                        </Button>
                    </ButtonToolbar>
                </section>
            </div>
        </>
    );
};