import fs from 'fs';
import path from 'path';
import express from 'express';
import reactRender from './react';
import extractFromManifest from './manifest';

const PORT = process.env.PORT || 3000;
const app = express();
const staticDir = path.join('dist', 'client');
const mediaDir = path.join('dist', 'data');
const manifest = JSON.parse(fs.readFileSync(path.join(staticDir, 'manifest.json')));
const [cssFiles, jsFiles] = extractFromManifest(manifest, ['css', 'js'])

app.use(process.env.PUBLIC_PATH, express.static(staticDir));
app.use('/uploads', express.static(mediaDir));
app.use('/*', (req, res) => {
    console.log(`REQ URL: ${req.originalUrl}`);
    return reactRender(req, res, {jsFiles, cssFiles});
});

app.listen(PORT, () => {
    console.log(`App started on ${PORT}`);
});