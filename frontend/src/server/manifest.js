export default (manifest, filesTypes) => filesTypes.map(fileType => {
    let extFiles = [];

    Object.keys(manifest.entrypoints).forEach(point => {
        if (fileType in manifest.entrypoints[point]) {
            extFiles = [...extFiles, ...manifest.entrypoints[point][fileType]];
        }
    });

    return extFiles;
});