import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { StaticRouter, matchPath } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import template from './template';
import routes from '../client/core/routes';
import App from '../client/App';

const findMatch = (url, routes) => {
    let matchRoutes = [];

    routes.forEach(route => {
        const matchData = matchPath(url, route);

        if (matchData) {
            if (route.exact) {
                matchRoutes.push({
                    component: route.component,
                    match: matchData
                });
            } else if (route.routes && route.routes.length) {
                matchRoutes = [...matchRoutes, ...findMatch(url, route.routes)];
            }
        }
    });

    return matchRoutes;
}

export default (req, res, params) => {
    const dataReqMatches = findMatch(req.originalUrl, routes).filter(mData => mData.component.getData);
    const dataReqFunctions = dataReqMatches.map(mData => mData.component.getData({match: mData.match}));

    Promise.all(dataReqFunctions).then(reqData => {
        const preloadedData = {};

        // Set Data to preloaded object
        dataReqMatches.forEach((mData, index) => {
            const key = mData.component.key;
            preloadedData[key] = reqData[index];
        });

        // Make HTML
        const appHtml = ReactDOMServer.renderToString(
            <StaticRouter location={req.originalUrl} context={{data: preloadedData}}>
                <App />
            </StaticRouter>
        );

        // Get Helmet data
        const helmet = Helmet.renderStatic();

        res.end(template({
            reactHtml: appHtml,
            helmet: helmet,
            preloadedData: preloadedData,
            ...params
        }));
    });
};