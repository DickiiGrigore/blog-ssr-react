
export default (params) => `
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
        ${params.helmet.title.toString()}
        ${params.helmet.meta.toString()}
        ${params.helmet.link.toString()}
        ${params.cssFiles.map(fileSrc => `<link rel="stylesheet" type="text/css" href="${fileSrc}"/>`)}
    </head>
    <body>
        <div id="root">${params.reactHtml}</div>
        <script>window.__DATA__ = ${JSON.stringify(params.preloadedData)}</script>
        ${params.jsFiles.map(fileSrc => `<script src="${fileSrc}"></script>`)}
    </body>
    </html>
`;